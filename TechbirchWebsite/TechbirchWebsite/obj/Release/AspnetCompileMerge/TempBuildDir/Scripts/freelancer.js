// Freelancer Theme JavaScript

(function($) {
    "use strict"; // Start of use strict
    /* ---------------------------------------------- /*
	 * Preloader
	/* ---------------------------------------------- */

    $(window).load(function () {
        $('#status').fadeOut();
        $('#preloader').delay(300).fadeOut('slow');
    });

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $("a[href*='#']").bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-up').fadeIn();
        } else {
            $('.scroll-up').fadeOut();
        }
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function(){ 
            $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    /* ---------------------------------------------- /*
		 * WOW Animation When You Scroll
		/* ---------------------------------------------- */

    var wow = new WOW({
        mobile: false
    });
    wow.init();

    /* ---------------------------------------------- /*
		 * Home BG
		/* ---------------------------------------------- */

    $(".screen-height").height($(window).height());

    $(window).resize(function () {
        $(".screen-height").height($(window).height());
    });

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('#home').css({ 'background-attachment': 'scroll' });
    } else {
        $('#home').parallax('50%', 0.1);
    }    

    // script for about us
    $('.flip').hover(function () {
        $(this).find('.card').toggleClass('flipped');
        return false;
    });

    $("div.serviceIcon").each(function(){
        $(this).click(function () {
            $(this).next().slideToggle("slow");
        });
    }); 

})(jQuery); // End of use strict
